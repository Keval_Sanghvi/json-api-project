const path = require('path');

module.exports = {
    entry: {
        app: ["@babel/polyfill", "./src/app.js"]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "app.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    devServer: {
        publicPath: '/dist/',
        contentBase: path.join(__dirname, '/dist'), // serve u all static files
        watchContentBase: true, // initiates page refresh if static content is changed!!!
        open: true,
        hot: true
    }
}

// polyfill in js basically means a workaround for something not available
/* acc to docs polyfill means something that will run before your source code so it should be a 
 dependency not a devDependency
*/
/*
    TO INSTALL BACKEND-SERVER :  
    * We have to hit npm install -g json-server.
        -> -g stands for global install.
    * We must create a file named db.json
    
*/
