class slHTTP {

    // get

    async get(url) {

        // fetch is default for get
        const res = await fetch(url);

        if (!res.ok) {
            await Promise.reject(new Error(res.status));
        }
        const data = await res.json();
        return data;
    }

    // post

    async post(url, data) {

        const res = await fetch(url, {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        if (!res.ok) {
            await Promise.reject(new Error(res.status));
        }
        const resData = await res.json();
        return resData;
    }

    // put

    async put(url, data) {

        const res = await fetch(url, {
            method: "PUT",
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        if (!res.ok) {
            await Promise.reject(new Error(res.status));
        }
        const resData = await res.json();
        return resData;
    }

    // delete

    async delete(url, data) {

        const res = await fetch(url, {
            method: "DELETE"
        });

        if (!res.ok) {
            await Promise.reject(new Error(res.status));
        }
        await res.json();
        return 'Post deleted...';
    }
}

const http = new slHTTP();
export default http;
