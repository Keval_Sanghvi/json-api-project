class UI {

    constructor() {

        this.post_title = document.getElementById("post_title");
        this.post_body = document.getElementById("post_body");
        this.post_author = document.getElementById("post_author");
        this.posts_container = document.querySelector(".posts-container");
        this.back = document.querySelector("#back");
        // dummy div where all posts will be inserted
        this.posts_wrapper = document.getElementById('posts');
    }

    showPosts(posts) {
        this.back.innerHTML = "";
        let output = "";
        posts.forEach(post => {
            output += `
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">${post.title}</h5>
                        <p class="card-text">${post.body}</p>
                        <a href="#" class="card-link author" data-id=${post.author}>${post.author}</a>
                        <a href="#" class="edit btn btn-primary pull-right" data-toggle="modal" data-target="#editModal" data-id=${post.id}>Edit</a>
                        <a href="#" class="delete btn btn-danger pull-right mr-3" data-id=${post.id}>Delete</a>
                    </div>
                </div>`;
        });
        this.posts_wrapper.innerHTML = output;
    }


    showAlert(message, classNames) {
        // clears existing alert if any
        this.clearAlert();
        const div = document.createElement('div');
        div.className = classNames;
        div.appendChild(document.createTextNode(message));
        this.posts_container.insertBefore(div, this.posts_wrapper);
        setTimeout(() => {
            this.clearAlert();
        }, 3000);
    }

    clearAlert() {
        const alertBox = document.querySelector(".alert");
        if (alertBox) {
            alertBox.remove();
        }
    }

    clearAllFields() {
        this.post_title.value = "";
        this.post_author.value = "";
        this.post_body.value = "";
    }

    fillModalData(post) {
        document.getElementById("edit_post_title").value = post.title;
        document.getElementById("edit_post_body").value = post.body;
        document.getElementById("edit_post_author").value = post.author;
        document.getElementById("edit_post_id").value = post.id;
    }

    showAuthorPosts(posts) {
        this.back.innerHTML = "<button type='button' class='btn btn-dark mb-3'>Get All Posts</button>";
        let output = "";
        posts.forEach(post => {
            output += `
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">${post.title}</h5>
                        <p class="card-text">${post.body}</p>
                        <a href="#" class="card-link">${post.author}</a>
                        <a href="#" class="edit btn btn-primary pull-right" data-toggle="modal" data-target="#editModal" data-id=${post.id}>Edit</a>
                        <a href="#" class="delete btn btn-danger pull-right mr-3 author" data-id=${post.id} data-author=${post.author}>Delete</a>
                    </div>
                </div>`;
        });
        this.posts_wrapper.innerHTML = output;
    }
}

const ui = new UI();
export default ui;
