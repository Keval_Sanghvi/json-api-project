import http from './slhttp';
import ui from './ui';

document.addEventListener('DOMContentLoaded', fetchPosts);

document.getElementById("add_post_btn").addEventListener('click', addPost);

// event delegation
document.getElementById("posts").addEventListener("click", deletePost);

// event delegation
document.getElementById("posts").addEventListener("click", editPost);

// event delegation
document.getElementById("posts").addEventListener("click", fetchAuthor);

document.getElementById("edit_post_btn").addEventListener("click", updatePost);

document.getElementById("back").addEventListener("click", back);

function fetchAuthor(e) {
    if (e.target.classList.contains("author")) {
        e.preventDefault();
        const author = e.target.dataset.id;
        http.get(`http://localhost:3000/posts?author=${author}`)
            .then(posts => ui.showAuthorPosts(posts))
            .catch(err => console.warn(err));
    }
}

function back() {
    fetchPosts();
}

function fetchAuthorPosts(author) {
    http.get(`http://localhost:3000/posts?author=${author}`)
        .then(posts => ui.showAuthorPosts(posts))
        .catch(err => console.warn(err));
}

// get all posts

function fetchPosts() {
    http.get("http://localhost:3000/posts")
        .then(posts => ui.showPosts(posts))
        .catch(err => console.warn(err));
}

// Add a new Post

function addPost() {
    const title = document.getElementById("post_title").value;
    const body = document.getElementById("post_body").value;
    const author = document.getElementById("post_author").value;

    const data = {
        title,
        body,
        author
    };

    http.post("http://localhost:3000/posts", data)
        .then(post => {
            ui.showAlert("Post Created Successfully!", "alert alert-success");
            ui.clearAllFields();
            fetchPosts();
        })
        .catch(err => console.warn(err));
}

// deletes a post

function deletePost(e) {
    if (e.target.classList.contains("delete")) {
        e.preventDefault();
        const id = e.target.dataset.id;
        const authorName = e.target.dataset.author;
        if (confirm("Are you sure you want to delete this post?")) {
            http.delete(`http://localhost:3000/posts/${id}`)
                .then(data => {
                    ui.showAlert("Post deleted successfully", "alert alert-info");
                    if (e.target.classList.contains("author")) {
                        fetchAuthorPosts(authorName);
                    } else {
                        fetchPosts();
                    }
                })
                .catch(err => console.warn(err));
        }
    }
}

function editPost(e) {
    if (e.target.classList.contains("edit")) {
        e.preventDefault();
        const id = e.target.dataset.id;

        // fetch data of post you want to edit

        http.get(`http://localhost:3000/posts/${id}`)
            .then(post => ui.fillModalData(post))
            .catch(err => console.warn(err));
    }
}

function updatePost() {
    const title = document.getElementById("edit_post_title").value;
    const body = document.getElementById("edit_post_body").value;
    const author = document.getElementById("edit_post_author").value;

    const editId = document.getElementById("edit_post_id").value;

    const data = {
        title,
        body,
        author
    };

    http.put(`http://localhost:3000/posts/${editId}`, data)
        .then(post => {
            ui.showAlert("Post Updated Successfully!", "alert alert-info");
            fetchPosts();
        })
        .catch(err => console.warn(err));

    // closing modal manually
    $("#editModal").modal("hide");
}
